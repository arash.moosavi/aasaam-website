export const memberConst = [
  {
    id: 'mhf',
    avatar: 'mhf.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/muhammad-hussein-fattahizadeh-30775839/',
      },
      {
        title: 'github',
        link: 'https://github.com/mhf-ir',
      },
      {
        title: 'stackoverflow',
        link: 'https://stackoverflow.com/users/306852/sweb',
      },
    ],
  },
  {
    id: 'moosavi',
    avatar: 'moosavi.svg',
    socialmedia: [],
  },
  {
    id: 'behzad',
    avatar: 'behzad.svg',
    socialmedia: [],
  },
  {
    id: 'ef',
    avatar: 'ef.svg',
    socialmedia: [],
  },
  {
    id: 'nazanin',
    avatar: 'nazanin.svg',
    socialmedia: [],
  },
  {
    id: 'neshom',
    avatar: 'neshom.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/neshom/',
      },
      {
        title: 'gitlab',
        link: 'https://gitlab.com/neshom/',
      },
    ],
  },
  {
    id: 'npm',
    avatar: 'npm.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'www.linkedin.com/in/narges-poormoghadasi',
      },
    ],
  },
  {
    id: 'artmary',
    avatar: 'artmary.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/maryam-haghgou-5755a0159/',
      },
      {
        title: 'github',
        link: 'https://github.com/artmarydotir',
      },
      {
        title: 'stackoverflow',
        link: 'https://stackoverflow.com/users/4494385/art-mary',
      },
    ],
  },
  // {
  //   id: 'tashakori',
  //   avatar: 'tashakori.svg',
  //   socialmedia: [],
  // },
  {
    id: 'arash',
    avatar: 'arash.svg',
    socialmedia: [],
  },
  {
    id: 'khashayar',
    avatar: 'khashayar.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/khashayar-mousavi-b8b713185/',
      },
    ],
  },
  {
    id: 'maani',
    avatar: 'maani.svg',
    email: '',
    socialmedia: [],
  },
  {
    id: 'ehsan',
    avatar: 'ehsan.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/ehsan-tashakkori-0443941b3/',
      },
    ],
  },
  {
    id: 'sara',
    avatar: 'sara.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/sara-piroozvand-1b70b5116',
      },
    ],
  },
  {
    id: 'fateme',
    avatar: 'fateme.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/fatemeh-tashakkori-2250116b/',
      },
    ],
  },
  {
    id: 'bahar',
    avatar: 'bahar.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/bahar-issari-a964071b3',
      },
    ],
  },
  {
    id: 'aidin',
    avatar: 'aidin.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'www.linkedin.com/in/aydin-nemati-5374421b3',
      },
    ],
  },
];
