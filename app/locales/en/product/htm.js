module.exports = {
  htm: 'HTM (HTTP Traffic Manager)',
  whatIsHtm: {
    title: 'What is an HTM system?',
    description:
      'The system is a network-edge web server that manages all HTTP traffic under the IT infrastructure of a collection. All the HTTP traffic of the network first passes through this server and then through this web server, the traffic is reduced to the backend web servers and they receive the response and transmit it to the clients. This system provides the following features:',
    items: [
      {
        title: 'WAF(Web Application Firewall)',
        desc:
          'This module allows HTTP requests to be security-checked before they reach the destination web servers, and if this traffic is suspected according to predefined rules or XSS attacks detect SQL injection…, traffic Blocks before it reaches the destination server.',
      },
      {
        title: 'DDOS and DOS prevention module',
        desc:
          'This module detects attacks called DOS attacks on the requesting Captcha user so that the user who is trying to destroy the system with the robot and increase the system load is not able to transfer the load to the destination server and disrupt the system.',
      },
      {
        title: 'Load Balancer module',
        desc:
          'This module allows the traffic of a destination server (Backend Server) to be transferred between several servers to share the load. For this purpose, various strategies such as IP Range based, HTTP Parameter Based, Round Robin, Failover,… can be used. This module can also create a cookie on the users browser that a user always receives service from the same server to which it is first connected.',
      },
      {
        title: 'SSL OFFLOADER module',
        desc:
          'Using this module, SSL Termination can be performed centrally for all systems behind HTM.',
      },
      {
        title: 'Page Speed module',
        desc:
          'Using this module, responses sent from backend servers to users browsers can be optimized. Among the optimizations that this module provides are the following:',
      },
    ],
    pageSpeed: [
      {
        title: 'Combine CSS',
        desc:
          'این ماژول با ترکیب فایل‌های css تعداد درخواست ها را در سمت مرورگر کاهش می‌دهد.',
      },
      {
        title: 'Combine Javascript',
        desc:
          'این ماژول با ترکیب فایل‌های Javascript  تعداد درخواست ها را در سمت مرورگر کاهش می‌دهد.',
      },
      {
        title: 'Collapse Whitespace',
        desc:
          'این ماژول با حذف کاراکترهای اضافی Space باعث کاهش حجم صفحه درخواستی خواهد شد.',
      },
      {
        title: 'Deduplicate Inlined Images',
        desc:
          'این ماژول عکس‌های Inline تکراری را تنها با یک درخواست فراخوانی می‌کند.',
      },
      {
        title: 'Extend Cache',
        desc:
          'این ماژول تلاش می‌کند که قابلیت Cache را برای منابع صفحه درخواستی کاربر به طوری فعال کند که اختلالی برای سامانه اصلی مورد درخواست ایجاد نشود.',
      },
      {
        title: 'Extend Cache PDFs',
        desc:
          'این ماژول قابلیت Cache را به طور خاص برای فایل‌های PDF فراهم می‌کند.',
      },
      {
        title: 'Image Filter and Option Reference',
        desc:
          'این ماژول برای بهینه سازی عکس‌های موجود در صفحه درخواستی به کار می‌رود. به طور مثال فایل‌های غیرمتحرک GIF را به فایلهای بهینه‌تر PNG تبدیل می‌کند. همچنین فایل‌های JPG با حجم زیاد را بهینه می‌کند، می‌تواند فایل‌های JPG را به فایل‌های بهینه‌تر WEBP بتدیل کند و...',
      },
      {
        title: 'Inline CSS',
        desc:
          'این ماژول فایل‌های کم حجم CSS را برای کاهش تعداد درخواست‌ها به‌ داخل فایل HTML منتقل می‌کند.',
      },
      {
        title: 'Inline JavaScript',
        desc:
          ' این ماژول فایل‌های کم حجم JavaScript را برای کاهش تعداد درخواست‌ها به‌ داخل فایل HTML منتقل می‌کند.',
      },
      {
        title: 'Lazyload Images',
        desc:
          'این ماژول برای بارگذاری سریعتر صفحات وب عکس‌هایی که در دیدرس کاربر نیستند را بارگذاری نمی‌کند و به محض اینکه عکس‌ها در دیدرس قرار گرفتند آن‌ها را بارگذاری می‌کند.',
      },
      {
        title: 'Responsive Images',
        desc:
          'این ماژول با اضافه کردن ویژگی srcset امکان قرار گیری نسخه‌های مختلفی از عکس را با تراکم پیکسلی متفاوت فراهم می‌کند.',
      },
      {
        title: 'Minify JavaScript',
        desc: 'این ماژول امکان متراکم سازی فایل‌های جاوا اسکریپت را فراهم می‌کند.',
      },
      {
        title: 'Pre-Resolve DNS',
        desc:
          ' این ماژول تکی را به صفحات HTML جهت Pre-Resolve DNS برای کاهش زمان ارتباط با DNS server را فراهم می‌کند.',
      },
    ],
  },
  whyHtm: {
    title: 'Why use HTM?',
    description: 'To answer this question, the following can be mentioned:',
    items: [
      {
        title: 'Security in the HTTP layer',
        desc:
          'The number of cyber attacks is increasing every year and the attacks are becoming more complex. Some attacks are carried out through HTTP requests, which can be largely prevented by timely detection and blocking the request from entering the destination server. Attacks such as SQL Injection and XSS are among the most popular and common attacks on the web, so that in total only these two types of attacks in 2020 more than 32% (https://www.acunetix.com/acunetix-web-application-vulnerability -report) account for all successful recorded attacks. Therefore, in collections that use various software, it will be very useful to use tools that have a serious role in preventing these attacks.',
      },
      {
        title: 'Prevent service disruption',
        desc:
          'One of the most common ways to sabotage DDOS attacks is by disrupting the service by creating too much traffic on the servers. DDOS attacks are often carried out through BOTs that, if the actual user requests the BOT request, can be detected to some extent to prevent malicious requests from occurring on the backend servers. For this purpose, having a good DDOS Protection system can prevent disruption and proper service.',
      },
      {
        title: 'Scalability and load sharing',
        desc:
          'In many cases, increasing the traffic and load of a system requires that it be possible to divide the load on several servers by horizontal scaling or increasing the number of servers. For this purpose, a good load balancer can play an effective role in this.',
      },
      {
        title: 'High Availability',
        desc:
          'Using Load Balancer gives IT suites the advantage that in the event of a problem for a server, with the detection of a problem by various types of Service Discovery, users do not go to the problem servers and receive only healthy web traffic servers.',
      },
      {
        title: 'Exchange layer security',
        desc:
          'One of the security requirements today is to use the HTTPS protocol for all web pages for Transport Layer security, which prevents man-in-middle attacks and is also effective against data breach. Therefore, having a central and secure system for SSL Termination operations is essential for any large IT suite.',
      },
      {
        title: 'Web page optimization',
        desc:
          'Web page optimization is important in two ways. First of all, the more optimized pages increase the page loading speed, and finally, it leads to a better user experience for users, and this in itself will make people satisfied with the use of the system. On the other hand, search engines give higher priority to pages that are optimized for speed and standardization, which higher priority in search engines will make pages more visible and users luck.',
      },
      {
        title: 'Check user side errors',
        desc:
          'Because user-friendly applications run on a variety of browsers, operating systems, and devices, examining user-generated errors and aggregating them will greatly enhance the quality of web-based applications.',
      },
    ],
  },
  howHtm: {
    title: 'What are the features of this system?',
    items: [
      {
        title: 'Scalability',
        desc:
          'This system can be implemented on a scalable basis. In this case, both the network and its processing power will be increased linearly and it will not be a Single Point of Failure.',
      },
      {
        title: 'Customization',
        desc:
          'This system can be optimally customized according to customer needs and cover other types of needs in this layer.',
      },
      {
        title: 'Monitoring system',
        desc:
          'This system is implemented along with a monitoring system to monitor its health and a log collection and processing system for Access Log, Error Log and Waf Log.',
      },
    ],
  },
  tech: {
    title: 'Technologies used',
  },
};
