module.exports = {
  cms: 'aasaam content managment system',
  whatIsCms: {
    title: 'What is aasaam content managment system',
    description:
      'It is a system that provides all the necessary facilities for a small news site to a large news site. Through it, journalists, photographers and secretaries of various services can upload and publish various content, including text, photos, videos, etc. on a site with different access levels. On the other hand, the content published for the audience, whether from PC or Available on mobile and tablet.',
    moduleTitle:
      'What modules does aasaam News Content Management System include? :',
    items: [
      {
        title: 'News Editorial Module',
      },
      {
        title: 'Ad stand module',
      },
      {
        title: 'Video advertising module',
      },
      {
        title: 'Poll module',
      },
      {
        title: 'Contact us module',
      },
      {
        title: 'Links module',
      },
      {
        title: 'Authors module',
      },
      {
        title: 'Companies / Organizations Module',
      },
      {
        title: 'Publications management module',
      },
      {
        title: 'Special letter module',
      },
      {
        title: 'Comment module',
      },
      {
        title: 'Module for users, groups and access levels',
      },
      {
        title: 'Tags management module',
      },
      {
        title: 'Editorial Performance Statistics Module',
      },
      {
        title: 'Site Visits Statistics Module',
      },
      {
        title: 'File, photo and video management module',
      },
      {
        title: 'Multimedia gallery module',
      },
      {
        title: '...',
      },
    ],
    buyCms:
      'For information on the full features and specifications of aasaam News Content Management System, contact our partners in the sales department.',
  },
  seoFriendly: {
    title:
      'Is a news site based on aasaam news content management system optimized for search engines (SEO)?',
    description:
      'Search engine optimization is a very frequent and complex discussion that includes a variety of strategies as well as a variety of technical and content levels. At the technical level, aasaam news content management system is optimized for search engines as much as possible. But the level of content is up to the content providers, and aasaam experts and colleagues will advise reporters and other content providers on search engine optimization to optimize content and news for search engines.',
  },
  speedFriendly: {
    title:
      'Is aasaam news content management system optimized for speed and efficiency?',
    description:
      'In terms of speed and efficiency, aasaam news content management system has been designed and optimized for hundreds of admin and reporter users, so that hundreds of reporters can use the system easily and with the best speed (if they have a suitable network and internet). The system is also designed for tens of thousands of viewers simultaneously and several million viewers per day.',
  },
  support: {
    title: 'What is aasaam technical support like?',
    description:
      'In case of emergencies such as malfunctions of the site or certain types, 24/7 aasaam technical experts provide services. Also, in the technical cases requested by the customers, it is possible to register the request through the "Customer Relations" panel, and the colleagues of the support department will respond to the requests as soon as possible.',
  },
  designSite: {
    title:
      'What is the process of designing a news site and news agency based on aasaam news content management system?',
    items: [
      {
        title: 'Introduction session and software demo',
      },
      {
        title:
          'Determining the structure of the site and customer needs in joint meetings',
      },
      {
        title: 'Basic graphic design and customer feedback',
      },
      {
        title: 'Resolve customer feedback and final approval',
      },
      {
        title:
          'Implementation of graphic design and connection to news management system',
      },
      {
        title: 'Data conversion if old data exists',
      },
      {
        title: 'Final testing and review',
      },
      {
        title: 'Training of journalists and editorial staff',
      },
      {
        title: 'Create basic data structure and define users',
      },
      {
        title: 'Final delivery',
      },
    ],
  },
  price: {
    title: 'What is the price of aasaam News Content Management System?',
    description:
      'To see the prices, refer to the following link. You can also contact our sales partners.',
  },
  start: {
    title: 'How do I start working together?',
    description:
      'Contact our partners in the sales department to answer any questions and tips.',
  },
  customer: {
    title: 'Who uses aasaam News System?',
    description: 'Visit aasaam to see customers.',
  },
  tech: {
    title:
      'What technologies has aasaam News Content Management System been provided with?',
  },
};
