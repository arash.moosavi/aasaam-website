const info = require('./info');
const job = require('./jobs');
const members = require('./member');
const htm = require('./product/htm');
const cms = require('./product/cms');
const dba = require('./solutions/dba');
const vtp = require('./solutions/vtp');
const monitoring = require('./solutions/monitoring');
const shahremohtava = require('./partners/shahremohtava');
const eima = require('./partners/eima');

module.exports = {
  aasaamjob: job,
  htmProduct: htm,
  cmsProduct: cms,
  dbaSolution: dba,
  vtpSolution: vtp,
  monitoringSolution: monitoring,
  partnerMohtava: shahremohtava,
  partnerEima: eima,
  teamMember: members,
  aasaamlink: info.url,
  contactPo: info.contactPoint,
  crm: 'Crm',
  head: {
    title: info.description,
    description: info.description,
    name: info.name,
    location: {
      latitude: info.location.geo.latitude.toString(),
      longitude: info.location.geo.longitude.toString(),
    },
    key: info.mainEntityOfPage.keywords,
  },
  about: {
    timelineHeader: 'We Have Come A Long Way',
    ourTeam: 'Meet Our Team',
    timeLine: {
      1390: {
        time: '2011',
        text:
          'A group of young people with high aspirations found each other in the first half of 2011 and came together to start a big business. Although they were inexperienced in starting a business, they were already experienced in technology. This youthful energy and energy was accompanied by the cooperation of more mature people who were able to form the core of aaasaam Software Group. At the beginning of the second half of 2011, the "aasaam News Content Management System" was launched.',
      },
      1391: {
        time: '2012',
        text:
          'It took a year to prepare the initial version of the aaasaam News Content Management System. After many security and performance tests on the system, we were able to run several news sites.',
      },
      1392: {
        time: '2013',
        text:
          '2013 was an important year in the work process of aaasaam, firstly, the technical team of aaasaam became bigger and more capable by attracting new staff, and also the passage of time and effort of the team, had made the software more mature. Experienced work was added to the aaasaam family.',
      },
      1393: {
        time: '2014',
        text:
          '2014 was the year of flowering of this 3-year-old seedling. The business team seriously entered the market of news and news sites. We participated in the press fair in the press-related businesses section, and many members of the media became acquainted with aaasaams services in several sessions during and after the fair.',
      },
      1394: {
        time: '2015 & 2016',
        text:
          'These were busy and stressful years for aaasaam. Both the technical team had to be expanded to meet the needs of customers and the news software had to be developed according to the needs of different customers. In these two years, dozens of news sites and news agencies were added to the family of aaasaams customers.',
      },
      1396: {
        time: '2017 & 2018',
        text:
          'After the success of aaasaam news content management software in the digital media market, we thought that it was time to take advantage of the experience of previous years to give more variety to the services of aaasaam Software Group and the potential created in recent years to become more active. So we defined different services and started working with a limited number of large organizations due to the current need of the IT market for modern DevOps services. Initially, these services were in the form of providing solutions and consulting, but then we implemented and implemented various solutions by getting acquainted with the different needs of customers in the field of DevOps.',
      },
      1398: {
        time: 'Now',
        text:
          'With the expansion of the technical team and resume of the collection, in addition to implementation, maintenance and consulting services and news and news sites, today with various services and products in the field of software development and DevOps services with several large and small private and public collections. we have. We hope to be able to expand the variety of our products even more every year by making the services and products of the past more reliable and up-to-date.',
      },
    },
  },
  backHome: 'Back to Home',
  pageNotFound: 'Page Not Found',
  otherError: 'An error accured.',
  legalName: info.legalName,
  aasaam: info.name,
  address: `${info.address.addressRegion} - ${info.address.addressLocality}`,
  email: info.email,
  identifier: [
    {
      title: 'National Identifier',
      value: info.identifier['0'].value,
    },
    {
      title: 'Economic Code',
      value: info.identifier['1'].value,
    },
    {
      title: 'Register Number',
      value: info.identifier['2'].value,
    },
  ],
  socialIcon: info.sameAs,
  telephone: info.telephone['0'],
  changeLanguagePost: 'Artículo disponible en español',
  soonLanguagePost: 'Artículo disponible en español pronto',
  comeBack: 'Come back',
  posts: 'Posts',
  home: 'Home',
  menu: {
    products: {
      cms: 'Content management system ( CMS )',
      waf: 'Http traffic Management (WAF,CDN)',
    },
    solutions: {
      vtp: 'VTP',
      monitoring: 'Monitoring',
      dba: 'DBA',
    },
    docs: {
      article: 'Article',
      faq: 'Faq',
      help: 'Help',
    },
    partner: {
      eima: 'Eima Agency',
      shahremohtava: 'Shahre Mohtava',
      iraniancmp: 'Iraninan information technololgy Co',
    },
  },
  review: 'Positive reviews by our satisfied clients',
  technologyUse: 'Our Technology',
  reviewQoute1:
    'On our own testimonials page, HubSpot features enthusiastic customer reviews alongside a note if that customer switched.',
  products: 'Products',
  solutions: 'Solutions',
  partners: 'Partners',
  documents: 'Documents',
  support: 'Support',
  followMetoAs: 'Routing to aasaam',
  callUs: 'Contact Us',
  sales: 'Sales',
  finance: 'Finance',
  salesPlan: 'See Plan',
  contactUs: 'Contact Us',
  contactUsMessage1:
    'aasaam support team with experienced and professional staff will responsibly support you',
  contactUsMessage2:
    'Our experts and support team answer questions and potential problems of customers and applicants. Contact our experts through the following ways.',
  whyUs: {
    title: 'Why our clients choose us',
    list: [
      {
        title: 'Dedicated and customized solutions',
        description:
          'According to the various needs in the field of information technology for different groups and organizations, we offer solutions that are designed and implemented based on the needs of each group.',
      },
      {
        title: 'Continuous and responsive support',
        description:
          'Continuous support and timely response are essential for our successful cooperation with our customers. That is why aasaam Support Unit strives to communicate responsibly and effectively with customers.',
      },
      {
        title: 'Always up to date',
        description:
          'The use of up-to-date and modern technologies in the world of information technology is an important element of success. At aasaam, we strive to always use the most modern solutions, taking into account the requirements.',
      },
    ],
  },
  aboutUs: 'About Us',
  welcomeMessage: 'Welcome to aasaam',
  welcomeMessage2:
    'We do not create stereotypes for your needs, but our solutions are clothes that are tailored to your needs.',
  welcomeDescription:
    'aasaam is a collection of motivated and professional young people who in todays technological world want to apply the solutions needed for success to each customer with the help of creativity and up-to-date knowledge. In this way, using the most modern solutions along with optimization, security, scalability and high reliability are the most important concerns.',
  footer: {
    footerMessage2: 'Let’s design your new ideas',
    footerMessage3: 'We’re Ready to Work with You',
    links: {
      termsOfService: 'Terms of Service',
      privacyPolicy: 'Privacy Policy',
      aasaamFamily: 'Our Team',
      jobs: 'We’re hiring',
    },
    companyAddress: 'Company Address',
  },
  helpLinks: 'Help Links',
  importanLinks: 'Important Links',
  telephoneNumber: 'Telephone Number',
  article: 'Article',
  introduce: {
    list: [
      {
        title:
          'Respond daily to more than 350 million requests in aasaam news content management system',
      },
      { title: 'Provide IoT based solutions' },
      { title: 'Consulting and implementation of data science platforms' },
      {
        title:
          'Providing services and solutions based on Waf, Load Balancing, SSL Offloading',
      },

      {
        title:
          'Provide devops solutions for enterprise IT development software teams and data centers',
      },
      { title: 'Web protection solutions' },
      {
        title:
          'Monitoring of more than 500 servers on the platform of aasaam monitoring system',
      },
      { title: 'Database management services' },
    ],
  },
  qouteCustomer: [
    {
      name: 'Alireza Bazartgani',
      position: 'Technical Manager of Gostaresh news',
      description:
        'The experience of working with aasaam has always been a great experience, and in addition to professional and excellent cooperation, you can always count on the quality of the software provided by Assam. Very good support and adherence to the principles of things that have always existed in all situations in cooperation with aasaam.',
    },
    {
      name: 'Sadegh Nakhjavani',
      position: 'Technical Manager of donya-e-eqtesad',
      description:
        'We have had many problems with news software systems in the collection for years. Familiarity with aasaam and working with this collection helped us to get rid of this concern forever and we can focus on the main advantage, namely content, and increase this cooperation every day. In my opinion, good support, moving on the edge of technology and more importantly, organizational culture and customer importance in aasaam team have been the most important factors for success.',
    },
  ],
};
