module.exports = {
  monitoring: 'Monitoring',
  whatIsMonitoring: {
    title: 'What is the monitoring process?',
    description:
      'In general, the observation of one or more ecosystems that collect, monitor and collect various events in different layers and then is made for the announcement by considering the rules that can be public or private, and according to the path of the port, Notify developer, maintainer or operator. To achieve this goal, several principles are needed, including data collection, pre-process monitoring, which includes sorting and structuring, sending / preparing for the integrated database. After sending and monitoring the post process, the data is stored in the integrated database and the alert system is activated, and according to the set rules, if necessary, the desired alert will be sent to the designated recipient.',
    monitorType: {
      title: 'Types of data in monitoring:',
      items: [
        {
          title: 'Metrics: Quantitative data and indicators',
        },
        {
          title:
            'Log events: All events recorded in different layers of software, services and operating systems and network hardware',
        },
      ],
    },
  },
  monitoringLayer: {
    title: 'What are the different layers of monitoring? ',
    description:
      'Data collection takes place in different layers, which is designed and implemented according to the needs and existing infrastructure. Some monitoring layers:',
    items: [
      {
        title:
          'Monitoring of environmental factors including power supply system, ambient temperature and humidity, condition of physical doors and…',
      },
      {
        title: 'Network monitoring and network equipment',
      },
      {
        title: 'Monitor operating systems on baremetal servers',
      },
      {
        title: 'Virtualization Software Monitoring (Vmware, KVM,…)',
      },
      {
        title: 'Monitor operating systems on virtual servers',
      },
      {
        title:
          'Monitoring of various services on the server - web server, databases and…',
      },
      {
        title: 'Monitoring proprietary software metrics and logs',
      },
      {
        title:
          'Web-based monitoring of internal and external organizational services',
      },
    ],
  },
  whyMonitoring: {
    title: 'Why have a central monitoring system?',
    description: 'The answer to this question can be explained in three ways.',
    items: [
      {
        key: '01',
        title: 'Multiple servers and services:',
        description:
          'Due to the complexity of IT infrastructures in different organizations and collections and the multiplicity of servers and services, a collection may use hundreds of servers and services, and monitoring the correctness of their functions is necessary. Because of this multiplicity, a central monitoring system and log along with a rapid alert system are required to provide accurate and precise service in any IT suite.',
        bg: 'work1',
      },
      {
        key: '02',
        title: 'Multiple monitoring layers:',
        description:
          'Checking and monitoring the performance of a system requires checking the performance of different layers in an IT suite. These layers are connected both vertically (from the peripheral monitoring layer to the operating system and services layer) and horizontally (web service-based communications). There is no choice but to have a central log and metric system to ensure the correct operation of the whole system and quick troubleshooting.',
        bg: 'service_item_03',
      },
      {
        key: '03',
        title: 'Different levels of monitoring and monitoring:',
        description:
          'In a IT suite, many roles such as IT management levels, network administrator, hardware manager, storage system manager, service manager, database manager, software developers, and نیاز each require their own level of monitoring and monitoring. They have to be able to check the correct operation of different parts related to themselves. A central monitoring system can allow monitoring of any role according to the level of access and need of that role.',
        bg: 'work1',
      },
    ],
  },
  howMonitoring: {
    title: 'How do we implement the monitoring system?',
    description: 'To answer this question, the following points can be mentioned:',
    items: [
      {
        title:
          'Understanding the current situation in terms of infrastructure and service and interviewing officials familiar with the entire infrastructure at different levels to calculate the design requirements of the monitoring system specifically for each complex.',
      },
      {
        title:
          'Select the software and services required to implement the monitoring system',
      },
      {
        title:
          'Continuous communication with different departments to create new monitoring items and create public and dedicated dashboards for each department',
      },
    ],
  },
  witchService: {
    title: 'What services do we monitor and monitor?',
    description:
      'The range and number of services and systems that can be monitored and monitored is very large, some of which are mentioned below:',
    slides: [
      {
        title:
          'Central log for Access Log and Error Logs of web servers such as Nginx, Apache and IIS',
      },
      {
        title:
          'The central log for Syslog for Linux operating systems includes the Debian and CentOS families',
      },
      {
        title: 'Central Log for Windows Event Logs',
      },
      {
        title: 'Central log for infringing firewalls',
      },
      {
        title:
          'Central log for various databases including Elasticsearch, MySQL and MSSQL',
      },
      {
        title:
          'Monitoring Windows and Linux operating systems (Memory, Disks, CPU, Swap, Page File,…)',
      },
      {
        title:
          'Monitoring of various network hardware including microtech, firewall, router, switch and…',
      },
      {
        title: 'Network Monitoring (Ping, Telnet,…)',
      },
      {
        title: 'DNS monitoring includes Bind, powerDNS, Microsoft DNS',
      },
      {
        title: 'Active Directory monitoring',
      },
    ],
  },
  tech: {
    title:
      'What technologies do we use to implement the monitoring and central log system?',
    description:
      'According to the requirements of each set of information technology and the prepared plan, we try to use the best technology in the world to design and implement a central monitoring system. Here are some of the technologies used in some projects that may use different combinations:',
  },
};
