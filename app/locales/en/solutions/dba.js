module.exports = {
  dba: 'Database Administrator (DBA)',
  whatIsDba: {
    title: 'What are database administrator services?',
    description:
      'Database maintenance services, which can be summarized as follows:',
    items: [
      {
        title:
          'Advice on choosing the right database for each project based on the required data model',
      },
      {
        title:
          'Consulting, installation and implementation of various databases in the most optimal way',
      },
      {
        title:
          'Consulting and implementing Clustering to make the database more accessible',
      },
      {
        title:
          'Consulting and implementing Clustering to increase system performance ',
      },
      {
        title:
          'Consulting, implementation and maintenance of backup and restore solutions (Backup & Restore)',
      },
      {
        title:
          'Consulting and implementation in database performance optimization (Performance Tuning)',
      },
      {
        title: 'Troubleshoot, troubleshoot, and implement solutions',
      },
    ],
  },
  whyDba: {
    title: 'Why use these services?',
    items: [
      {
        title:
          'Today, with the great variety in the needs of different systems as well as the emergence of databases with data modeling and very diverse features, choosing the appropriate databases is one of the most essential points for the success of a project.',
      },
      {
        title:
          'Proper and standard implementation, especially coordination with the rest of the software stack, guarantees the correct operation of the software in general. Therefore, optimal installation and implementation of the database is a necessity for software development',
      },
      {
        title:
          'Due to the needs of the software under development as well as the features of different databases, scalability plan design is very important according to scientific data such as CAP theory and.',
      },
      {
        title:
          'Given the sensitivity of the data, the accurate and correct design and implementation of backup and reset policies should be one of the most critical steps for any IT suite.',
      },
      {
        title:
          'The optimization process at every moment of software development should be considered by the development team and operations team of each IT suite. Database optimization according to the software workload (Software Workload) is one of the most important parts of any software optimization process.',
      },
    ],
  },
  howDba: {
    title: 'What databases do we support?',
    description:
      'According to the work experience of this collection, including the bases that are considered in these services, the following can be mentioned:',
    items: [
      {
        title: 'SQL-based databases (RDBMS):',
        list: ['MySQL – MariaDB – Percona', 'PostgrsSQL', 'Microsoft SQL Server'],
      },
      {
        title: 'NOSQL Databases:',
        list: [
          'MongoDB',
          'Elasticsearch – Open Distro for Elasticsearch',
          'ArangoDB',
          'Redis',
          'InfluxDB',
        ],
      },
    ],
  },
};
