module.exports = {
  title: 'Eima story began with a question: "Is propaganda art or science?"',
  description:
    'Years of experience and activity in Ima have taught us that advertising is both art and science, and "Ima" is an artistic reference to the science of advertising. , We create businesses and collections that want to experience a new kind of creative advertising. At IMA, relying on the power of our experts, without default, after needs assessment and ideation, by developing creative strategies, to achieve the best possible result, we provide the most modern services in the fields of content production, branding, digital marketing and public relations. We offer all collections. "Our specialty is the artistic narrative of modern advertising science."',
  services: {
    title: 'Eima Services:',
    items: [
      {
        title: 'Industrial and advertising photography',
        desc:
          'Applying the art of photography to advertising creates a lasting image that can have the greatest impact on the audience.',
      },
      {
        title: 'Advertising teaser and industrial documentary',
        desc:
          'Advertising teaser is a short film, to promote and influence the audience. Combining art and creative advertising.',
      },
      {
        title: 'Motion graphics',
        desc:
          'Motion graphics is a type of filmmaking that uses animated graphics to produce it. In these works, the two arts of filmmaking and graphics are combined with each other and the capabilities of both to convey content and message are used.',
      },
      {
        title: 'Graphic Design',
        desc:
          'Design is a conscious effort to create a productive order, in order to best present products and services. Artistic order in the context of advertising.',
      },
      {
        title: 'Digital Marketing',
        desc:
          'Digital marketing includes a set of tools and activities that are used to market digital products and services.',
      },
      {
        title: 'Stop Motion',
        desc:
          'Stop motion is a method of producing motion pictures or animations that is achieved by sequentially shooting objects and making small movements between frames. Displaying frames at high speeds makes objects appear to be moving.',
      },
      {
        title: 'Content production',
        desc:
          'Content is informative information that informs the audience, interacts with the audience, and is engaging to the audience.',
      },
      {
        title: 'public relations',
        desc:
          'Public relations is a set of calculated actions and activities that each organization takes to establish effective and purposeful communication with the groups with which it is associated.',
      },
    ],
  },
};
