module.exports = {
  title: 'IRITCO',
  description:
    'Iranian Information Technology Company (IRITCO) was established in 1379 in a completely private manner, with the aim of working in new fields of information technology. Since 2007, with the increase in the volume and variety of organizational data and the revelation of their value in the strategy and development path of organizations and firms, the presentation of new solutions in the field of data storage was considered by company managers. Iranian Information Technology Company, by attracting talented, entrepreneurial and expert forces in this field and aiming to keep up with the latest scientific and information technology developments, is currently specialized in providing integrated information and communication technology solutions and has been able to provide worthy services to the most prominent organizations and Provide public and private companies.',
  whatIsIraninan: {
    title: 'The main areas of activity of this company are:',
    items: [
      {
        title: 'Provide a comprehensive data storage solution',
        sublist: [
          {
            title:
              'Design, supply and implementation of data storage infrastructure',
          },
          {
            title: 'Support and maintenance of data storage infrastructure',
          },
          {
            title: 'Monitoring data storage infrastructure',
          },
          {
            title: 'Provide a backup solution for data',
          },
          {
            title: 'Provide a solution to deal with the crisis (Disaster Recovery)',
          },
          {
            title: 'Provide a Business Continuity solution',
          },
        ],
      },
      {
        title:
          'Integrated design and implementation of information technology infrastructure and weak current systems',
        sublist: [
          {
            title: 'Cable, wireless and GPON network infrastructure',
          },
          {
            title: 'Design and implementation of a standard data center',
          },
          {
            title: 'Video surveillance infrastructure',
          },
          {
            title: 'IP Phone and IPTV infrastructure',
          },
        ],
      },
      {
        title:
          'Provide solutions for monitoring and managing information and communication technology resources',
        sublist: [
          {
            title:
              'Comprehensive monitoring solution for information technology resources',
          },
          {
            title: 'Integrated identity management solution in the organization',
          },
        ],
      },
    ],
  },
  keywords: {
    title: 'Important Notes',
    items: [
      {
        title: 'History of major EPC projects',
      },
      {
        title: 'Obtaining more than 70 customer testimonials',
      },
      {
        title:
          'Provide specialized solutions and projects for customers in various fields',
        sublist: [
          {
            title: 'Banking area',
          },
          {
            title: 'Academic / Research Area',
          },
          {
            title: 'Hospital area',
          },
          {
            title: 'Oil, gas and petrochemical companies',
          },
          {
            title: 'ICT field ',
          },
          {
            title: 'Road and urban development',
          },
        ],
      },
    ],
  },
};
