const info = require('./info');
const job = require('./jobs');
const members = require('./member');
const htm = require('./product/htm');
const cms = require('./product/cms');
const dba = require('./solutions/dba');
const vtp = require('./solutions/vtp');
const monitoring = require('./solutions/monitoring');
const shahremohtava = require('./partners/shahremohtava');
const eima = require('./partners/eima');

module.exports = {
  aasaamjob: job,
  htmProduct: htm,
  cmsProduct: cms,
  dbaSolution: dba,
  vtpSolution: vtp,
  monitoringSolution: monitoring,
  partnerMohtava: shahremohtava,
  partnerEima: eima,
  teamMember: members,
  aasaamlink: info.url,
  contactPo: info.contactPoint,
  crm: 'سامانه ارتباطی',
  call: 'تماس بگیرید',
  ourworks: 'نمونه کارها',
  head: {
    title: info.description,
    description: info.description,
    name: info.name,
    location: {
      latitude: info.location.geo.latitude.toString(),
      longitude: info.location.geo.longitude.toString(),
    },
    key: info.mainEntityOfPage.keywords,
  },
  about: {
    timelineHeader: 'ما راهی طولانی را با یک چشم ‏انداز روشن پیموده‏‌ایم',
    ourTeam: 'با خانواده ی آسام آشنا شوید',
    timeLine: {
      1390: {
        time: '۱۳۹۰',
        text:
          'جمعی جوان که ارزوهای بلندی در سر داشتند در نیمه اول سال ۱۳۹۰ یکدیگر را پیدا کردند و جمعی شدند برای شروع یک کار بزرگ.هر چند در عرصه ی راه اندازی کسب و کار کم تجربه بودند اما در عرصه های تکنولوژیک پیش از ان طبعی ازموده بودند.این نیروی و انرژی جوانی با همراهی و همفکری افراد پخته تری همراه بود که توانست هسته ی اولیه گروه نرم افزاری آسام را شکل بدهد.در ابتدای نیمه دوم سال ۱۳۹۰ کلنگ شروع کار  «سیستم مدیریت محتوای خبری آسام» زده شد.',
      },
      1391: {
        time: '۱۳۹۱',
        text:
          'یکسالی طول کشید تا نسخه اولیه «سیستم مدیریت محتوای خبری آسام» اماده شود. پس از تست های فراوان امنیتی و کارایی روی سیستم توانستیم اجرای چند سایت خبری را به عهده بگیریم.',
      },
      1392: {
        time: '۱۳۹۲',
        text:
          'سال ۱۳۹۲  سال مهمی در روند کاری آسام بود، اول اینکه تیم فنی آسام با جذب نیروهای جدید بزرگتر و تواناتر شد و همچنین گذر زمان و تلاش تیم، نرم افزار را پخته تر کرده بود. همچنین در این سال تیم بارزگانی کوچک اما حرفه ای و کار ازموده به خانواده آسام اضافه شد.',
      },
      1393: {
        time: '۱۳۹۳',
        text:
          'سال ۱۳۹۳ سال گل دادن این نهال ۳ ساله بود. تیم بازرگانی به طور جدی وارد بازار سایت های خبری و خبرگذاری شد. در نمایشگاه مطبوعات در بخش کسب و کارهای مرتبط با مطبوعات شرکت کردیم و بسیاری از اهالی رسانه در جلسات متعدد حین نمایشگاه و پس از ان با خدمات آسام اشنا شدند.',
      },
      1394: {
        time: '۱۳۹۴ و ۱۳۹۵',
        text:
          'این سال ها سالهایی پر کار و پر فشار برای آسام بود. هم باید تیم فنی برای پاسخگویی به نیازهای مشتریان گسترش پیدا میکرد و هم نرم افزار خبری باید طبق نیاز های مشتریان مختلف توسعه می یافت. در این دو سال ده ها سایت خبری و خبرگزاری به خانواده مشتریان آسام اضافه شد.',
      },
      1396: {
        time: '۱۳۹۶ و ۱۳۹۷',
        text:
          'پس از موفقیت نرم افزار مدیریت محتوای خبری آسام در بازار رسانه های دیجیتال به این فکر بودیم که وقت ان رسیده که با بهره گیری از تجربه ی سالهای گذشته به خدمات گروه نرم افزاری آسام تنوع بیشتری بدهیم و پتانسیل ایجاد شده در سالهای گذشته به فعلیت بیشتری برسانیم. بنابراین خدمات مختلفی را تعریف کردیم و با توجه به نیاز روز بازار فناوری اطلاعات به خدمات مدرن DevOps همکاری با تعداد محدودی سازمان بزرگ را اغاز کردیم. در ابتدا این خدمات به صورت ارائه راه حل و مشاوره بود که در ادامه با آشنایی با نیاز های مختلف مشتریان در حوزه DevOps راه حل های مختلفی را پیاده سازی و اجرا کردیم.',
      },
      1398: {
        time: 'امروز',
        text:
          'با گسترش تیم فنی و رزومه کاری مجموعه، علاوه بر خدمات پیاده سازی، نگهداری و مشاوره و سایت های خبری و خبرگذاری، امروز با خدمات و محصولات متعدد حوزه توسعه نرم افزار و خدمات DevOps با چندین مجموعه بزرگ و کوچک خصوصی و دولتی همکاری های متعددی داریم. امیدواریم بتوانیم هر سال با اطمینان پذیرتر کردن (Stability) و بروز کردن خدمات و محصولات گذشته بتوانیم، تنوع محصولاتمان را نیز بیش از پیش گسترس دهیم.',
      },
    },
  },
  backHome: 'بازگشت به خانه',
  pageNotFound: 'صفحه ی مورد نظر شما یافت نشد',
  otherError: 'پاسخگویی با مشکلی مواجه شده است.',
  aasaam: info.name,
  address: `${info.address.addressRegion} - ${info.address.addressLocality}`,
  email: info.email,
  identifier: [
    {
      title: 'شناسه ملی',
      value: info.identifier['0'].value,
    },
    {
      title: 'کد اقتصادی',
      value: info.identifier['1'].value,
    },
    {
      title: 'شماره ثبت',
      value: info.identifier['2'].value,
    },
  ],
  socialIcon: info.sameAs,
  telephone: info.telephone['0'],
  legalName: info.legalName,
  changeLanguagePost: 'تغییر زبان',
  soonLanguagePost: 'زبان پست جدید',
  comeBack: 'بازگشت',
  posts: 'پست',
  home: 'خانه',
  menu: {
    products: {
      cms: 'سیستم مدیریت خبری ( CMS )',
      waf: 'سامانه مدیریت ترافیک HTTP',
    },
    solutions: {
      vtp: 'راه حل های کلاینت های سازمانی (VTP)',
      monitoring: 'مانیتورینگ',
      dba: 'خدمات مدیریت پایگاه داده',
    },
    docs: {
      article: 'مقالات',
      faq: 'پرسش های متداول',
      help: 'راهنما',
    },
    partner: {
      iraniancmp: 'فناوری اطلاعات ایرانیان',
      eima: 'استودیو تبلیغات خلاق ایما',
      shahremohtava: 'شهر محتوا',
    },
  },
  review: 'پیام های مشتریان ما',
  technologyUse: ' فناوری‌هایی که ما در آسام استفاده می‌کنیم ',
  reviewQoute1:
    'آسام تیمی  است که  تمام آنچه برای ساختن رویای خود نیاز دارید را به تبدیل میکند.',
  products: 'محصولات',
  solutions: 'راهکار ها',
  partners: 'شرکت های همکار',
  documents: 'مستندات',
  support: 'پشتیبانی',
  callUs: 'تماس با ما ',
  sales: 'فروش',
  finance: 'مالی',
  salesPlan: 'مشاهده ی تعرفه ها',
  information: 'ارتباطات',
  fax: 'فکس',
  followMetoAs: 'مسیریابی به آسام',
  contactUsMessage1:
    'تیم پشتیبانی آسام با کادری مجرب و حرفه ای به صورت مسئولانه پشتیان شما خواهند بود',
  contactUs: 'با ما تماس بگیرید',
  contactUsMessage2:
    'کارشناسان و تیم پشتیبانی ما ، پاسخ‌گوی پرسش‌ها و مشکلات احتمالی مشتریان و متقاضیان هستند. برای برقراری ارتباط با کارشناسان ما از طرق زیر ارتباط بگیرید.',
  whyUs: {
    title: 'چرا آسام را انتخاب کنیم؟',
    list: [
      {
        title: 'راهکارهای اختصاصی و سفارشی سازی شده',
        description:
          'با توجه به نیازمندی‌های گوناگون و متنوع در حوزه‌ی فناوری اطلاعات برای مجموعه‌ها و سازمان‌های مختلف راهکارهایی را ارائه می‌دهیم که بر اساس نیازمندی‌های اختصاصی هر مجموعه طراحی و پیاده سازی شده است.',
      },
      {
        title: 'پشتیبانی مستمر و پاسخگو',
        description:
          'پشتیبانی مستمر و پاسخگویی به موقع لازمه ی همکاری موفق ما با مشتریان ما است. به همین جهت واحد پشتیبانی آسام  تلاش می کند تا بصورت مسئولانه و موثر با مشتریان در ارتباط باشد.',
      },
      {
        title: 'همیشه به روز',
        description:
          'استفاده از تکنولوژی های به روز و مدرن در دنیای فناوری اطلاعات از عناصر مهم موفقیت است. ما در آسام تلاش می کنیم که همواره با درنظر گرفتن نیازمندی ها از مدرن ترین راه حل ها استفاده کنیم.',
      },
    ],
  },
  aboutUs: 'درباره آسام',
  welcomeMessage: 'به آسام خوش آمدید',
  welcomeMessage2:
    'ما کلیشه ای برای نیازهای شما نمیسازیم، بلکه راه حل های ما لباسی است که به تن نیازهای شما دوخته شده',
  welcomeDescription:
    'ما در آسام در تلاش هستیم که با شناخت هر چه بیشتر نیازهای مشتریان و بهره‌گیری از خلاقیت و دانش روز راه حل متناسب را برای موفقیت مشتریان خود به کار گیریم. در این راه استفاده از مدرن‌ترین راه حل ها در کنار بهینه بودن (Performance)، امنیت (Security)، مقیاس‌پذیری (Scaling) و قابلیت اطمینان بالا (Reliability) از مهم ترین دغدغه های ماست.',
  footer: {
    footerMessage1:
      'مشاوره و ارائه‌ راه حل‌هایی در زمینه‌های نرم‌افزارهای تحت وب، ساخت برنامه‌های موبایل‌ ، بهینه‌ سازی موتورهای جستجو، امنیت وب، تهیه و نگه‌داری سرور، تأمین محتوا، بازاریابی، تبلیغات و جذب بازدیدکننده ',
    footerMessage2: 'فقط کافیست از طریق یکی از راه های ارتباطی با ما تماس بگیرید.',
    footerMessage3: 'برای شروع یک پروژه جدید آماده اید؟ ',
    links: {
      termsOfService: 'شرایط استفاده از خدمات',
      privacyPolicy: 'سیاست حفظ حریم خصوصی',
      aasaamFamily: 'خانواده آسام',
      jobs: 'فرصت های شغلی',
    },
    companyAddress: 'آدرس ما',
  },
  helpLinks: 'لینک های کمکی',
  importanLinks: 'لینک های مهم',
  telephoneNumber: 'شماره تماس',
  plans: 'تعرفه ها',
  planMessage1: 'بسته مناسب خود را انتخاب کنید!',
  planMessage2:
    'گروه نرم افزاری آسام دادای تعرفه هایی برای محصول سیستم مدیریت خبری میباشد.پس از انتخاب تعرفه ی خود با ما تماس بگیرید.',
  article: 'مقالات',
  introduce: {
    list: [
      {
        title: 'ارائه راه حل جامع تحریریه و سیستم مدیریت محتوای خبری آسام',
      },
      { title: 'ارائه راهکار های مبتنی بر IoT' },
      {
        title:
          'راه حل جامع و یکپارچه مانیتور سرویس ها و منابع زیرساخت فناوری اطلاعات',
      },
      {
        title:
          ' ارائه خدمات و راهکارهای مبتنی بر Waf, Load Balancing, SSL Offloading ',
      },
      {
        title:
          'ارائه راهکارهای دواپس برای تیم های توسعه ی نرم افزاری زیرساخت های فناوری اطلاعات سازمانی و مراکز داده',
      },
      { title: 'مشاوره و پیاده سازی راهکار های حفاظتی سامانه های تحت وب' },

      { title: 'مشاوره و پیاده سازی پلتفرم های علوم داده' },
      { title: 'خدمات مدیریت پایگاه داده' },
    ],
  },
  qouteCustomer: [
    {
      name: 'علیرضا بازرگانی',
      position: 'مدیر فنی گسترش نیوز',
      description:
        'تجربه همکاری با آسام همیشه یک تجربه‌ی عالی بوده و در کنار همکاری حرفه‌ای و عالی همیشه میتونید روی کیفیت نرم‌افزار ارائه شده توسط آسام حساب کنید. پشتیبانی بسیار خوب و رعایت اصول از مواردی هست که همیشه در همه شرایط توی همکاری با آسام وجود داشته.',
    },
    {
      name: 'صادق نخجوانی',
      position: 'مدیر فنی مجموعه دنیای اقتصاد',
      description:
        'سالها بود که در خصوص سیستم‌های نرم‌افزاری خبری در مجموعه دچار مشکلات زیادی بودیم. آشنایی با آسام و کار با این مجموعه با عث شد این دغدغه برای همیشه مرتفع بشه و ما بتونیم روی مزیت اصلی یعنی محتوا تمرکز کنیم و هر روز این همکاری رو بیشتر کنیم. به نظر من پشتیبانی خوب، حرکت روی لبه تکنولوژی و مهمتر از اون فرهنگ سازمانی و اهمیت به مشتری در تیم آسام مهمترین عامل موفقیت بوده.',
    },
  ],
};
