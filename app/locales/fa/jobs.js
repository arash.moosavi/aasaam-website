module.exports = {
  title:
    ' گروه نرم افزاری آسام برای توسعه زیرساخت های فنی نیازمند متخصصین تمام می‌باشد',
  join: 'به ما ملحق شوید',
  jobMsg1: 'با ما همکار شوید',
  jobMsg2:
    'گروه نرم افزاری آسام قصد دارد جهت تکمیل سرمایه‌های انسانی خود در توسعه ی زیرساخت هایش نسبت به جذب کارشناسان حوزه فناوری اطلاعات با توانمندی‌های زیر اقدام نماید.',
  jobMsg3:
    'دستیابی به هدف‌های بزرگ تنها با ایجاد یک تیم فوق العاده امکان پذیر میشود. افرادی که جویای کار در محیطی بسیار صمیمی و آرام هستند و دارای شرایط لازم در مورد یکی از موقعیت های شغلی درخواست شده هستند و در ثابت نمودن مهارت های خود مطمئن هستند میتوانند رزومه کامل و بروز شده خود را به آدرس ایمیل  info@aasaam.com ارسال کنند. در خانواده آسام منتظر شما هستیم، به ما ملحق شوید.',
  benefits: {
    title: 'مزایا',
    item: [
      {
        title: 'صبحانه',
      },
      {
        title: 'محیط کاری دوستانه',
      },
      {
        title: 'حقوق رقابتی',
      },
      {
        title: 'پرداخت به موقع',
      },
      {
        title: 'بیمه عادی و تکمیلی',
      },
      {
        title: 'کار با فن‌آوری‌های به بروز',
      },
      {
        title: 'یادگیری از سایرین و آموزش به سایرین',
      },
    ],
  },
  call: 'راه های تماس',
  positions: [
    {
      title:
        '👨🏽‍💻👩🏻‍💻 گروه نرم‌افزاری آسام در حال حاظر به دنبال فردی خلاق و با پشتکار، جهت پوزیشن فرانت اند(Front-end) می‌گردد: ',
      list: {
        all: {
          title: 'مهارت‌های ضروری:',
          item: [
            'Hands-on Experience with Git',
            'Experience with Node.js environment (npm, yarn)',
            'Knowlege of REST and GraphQL',
            'Experience with Vue.js (Framework like Vuetify)',
            'Some experience with SSR and Nuxt.js',
          ],
        },
        ideally: {
          title: 'مهارت‌های ایده‌آل:',
          item: [
            'Linux, Bash',
            'CI and container environment',
            'Knowlege of REST and GraphQL',
            'Testing approach like TDD and BDD, Unit and E2E',
            'Scrum with Agile team',
          ],
        },
        personal: {
          title: 'مهارت های شخصی:',
          item: [
            'Fluent in English, reading and listening',
            'Proven track record of always learning',
            'Proactive attitude',
          ],
        },
      },
    },
    {
      title:
        '👨🏽‍💻👩🏻‍💻 گروه نرم‌افزاری آسام در حال حاظر به دنبال فردی خلاق و با پشتکار، جهت پوزیشن بک اند(Back-end) می‌گردد:',
      list: {
        all: {
          title: 'مهارت‌های ضروری:',
          item: [
            'Hands-on Experience with Linux, Git, Docker and Bash',
            'Experience with CI and Container Environment',
            'Great Experience with Node.js as Backend developer',
            'Good knowledge about REST, GraphQL',
            'knowledge and Experience with MariaDB, Postgres, MongoDB, ElasticSearch, Redis, InfluxDB',
          ],
        },
        ideally: {
          title: 'مهارت‌های ایده‌آل:',
          item: [
            'Fastify, Molecular',
            'CI and container environment',
            'Testing approach like TDD, Unit and Integration',
            'Scrum with Agile team',
          ],
        },
        personal: {
          title: 'مهارت های شخصی:',
          item: [
            'Fluent in English, reading and listening',
            'Proven track record of always learning',
            'Proactive attitude',
          ],
        },
      },
    },
    {
      title:
        '👨🏽‍💻👩🏻‍💻 گروه نرم‌افزاری آسام در حال حاظر به دنبال فردی خلاق و با پشتکار، جهت پوزیشن دواپس(DevOps) می‌گردد:',
      list: {
        all: {
          title: 'مهارت‌های ضروری:',
          item: [
            'Hands-on Experience with Linux, Git, Docker, Python and Bash',
            'Experience with automation like Ansible and SaltStack',
            'IaC in Mind',
            'Good knowledge about Network topology',
            'Good knowledge about Bare metal',
            'knowledge about and Experience with Windows Server, Linux Server(Mostly Debian Based)',
          ],
        },
        ideally: {
          title: 'مهارت‌های ایده‌آل:',
          item: [
            'Prometheus, ELK Stack, TICK Stack',
            'Metric collectors like Telegraf and Prometheus Exporters',
            'Log collectors like fluntbit and vector',
            'Grafana, Kibana and Modern analytic dashboard',
            'Consul, Nginx',
            'Scrum with Agile team',
          ],
        },
        personal: {
          title: 'مهارت های شخصی:',
          item: [
            'Fluent in English, reading and listening',
            'Proven track record of always learning',
            'Proactive attitude',
          ],
        },
      },
    },
  ],
};
