module.exports = {
  title: 'داستان ایما با یک پرسش آغاز شد: «تبلیغات هنر است یا علم؟»',
  description:
    'سال‌ها تجربه و فعالیت در ایما به ما آموخت که تبلیغات هم هنر است و هم علم و «ایما» اشاره‌ایست هنرمندانه به علم تبلیغات.امروز ما در «آژانس تبلیغات خلاق ایما» با تلفیق هنر و علم، فکر و خلاقیت را به خدمت بنگاه‌ها، کسب و کارها و مجموعه‌هایی در می‌آوریم که می‌خواهند نوع جدیدی از تبلیغات خلاق را تجربه کنند. ما در ایما، با تکیه بر توان متخصصان خود، بدون پیش فرض، پس از نیازسنجی و ایده‌پردازی، با تدوین استراتژی‌های خلاقانه، برای رسیدن به بهترین نتیجه ممکن، مدرن‌ترین خدمات را در حوزه‌های تولید محتوا، برندینگ، دیجیتال مارکتینگ و روابط عمومی، به تمامی مجموعه‌ها ارائه می‌دهیم. «تخصص ما روایتیست هنرمندانه از علم تبلیغات مدرن.»',
  services: {
    title: 'خدمات ایما:',
    items: [
      {
        title: 'عکاسی صنعتی و تبلیغاتی',
        desc:
          'به خدمت گرفتن هنر عکاسی برای تبلیغات، تصویری ماندگار می‌سازد که می‌تواند بیشترین تاثیر را بر روی مخاطب داشته باشد.',
      },
      {
        title: 'تیزر تبلیغاتی و مستند صنعتی',
        desc:
          'تیزر تبلیغاتی، فیلم کوتاهی است، برای تبلیغ و تاثیر بر مخاطب. تلفیق هنر و تبلیغات خلاق.',
      },
      {
        title: 'موشن گرافیک',
        desc:
          'موشن گرافیک گونه‌ای از فیلم سازیست که برای تولید آن از گرافیک متحرک استفاده می‌شود.در این آثار دو هنر فیلم سازی و گرافیک با یکدیگر تلفیق شده و از قابلیت‌های هر دو جهت انتقال محتوا و پیام استفاده می‌شود.',
      },
      {
        title: 'طراحی گرافیک',
        desc:
          'طراحی تلاشی آگاهانه است برای ایجاد نظمی نتیجه بخش، به منظور بهترین ارایه از محصولات و خدمات. نظمی هنری در بستر تبلیغات.',
      },
      {
        title: 'دیجیتال مارکتینگ',
        desc:
          'بازاریابی دیجیتال شامل مجموعه همه ابزارها و فعالیت‌هایی است که برای بازاریابی محصولات و خدمات در بستر دیجیتال مورد استفاده قرار می‌گیرند.',
      },
      {
        title: 'استاپ موشن',
        desc:
          'استاپ موشن (Stop Motion) یکی از روش‌های تولید فیلم‌های متحرک یا انیمیشن است که با تصویر برداری پی در پی از اجسام و انجام حرکت‌های جزئی بین فریم‌ها به دست می‌آید. نمایش فریم‌ها با سرعت زیاد باعث می‌شود تا به نظر برسد اجسام در حال حرکت هستند.',
      },
      {
        title: 'تولید محتوا',
        desc:
          'محتوا اطلاعات ترغیب کننده‌ای هست که به مخاطب اطلاع رسانی می‌کند، با مخاطب تعامل ایجاد می‌کند و برای مخاطب جذاب است.',
      },
      {
        title: 'روابط عمومی',
        desc:
          'روابط عمومی مجموعه‌ای از اقدامات و فعالیت­های حساب‌شده‌ای است که هر سازمان برای برقراری ارتباطات موثر و هدفمند با گروه‌هایی که با آن در ارتباطند انجام می‌دهد.',
      },
    ],
  },
};
