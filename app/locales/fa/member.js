module.exports = {
  artmary: {
    name: 'مریم حقگو',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  mhf: {
    name: 'محمدحسین فتاحی زاده',
    role: 'مدیر ارشد فناوری',
    description: '',
  },
  neshom: {
    name: 'محسن توحیدی پور',
    role: 'مدیر ارشد ارتباطات',
    description: '',
  },
  moosavi: {
    name: 'آرش موسوی',
    role: 'مدیر عامل',
    description: '',
  },
  behzad: {
    name: 'بهزاد نورایی',
    role: 'مدیر ارشد بازرگانی',
    description: '',
  },
  ef: {
    name: 'الهه فاضل',
    role: 'مدیر ارشد مالی',
    description: '',
  },
  npm: {
    name: 'نرگس پورمقدسی',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  nazanin: {
    name: 'نازنین زارع',
    role: 'کارشناس ارتباطات',
    description: '',
  },
  // tashakori: {
  //   name: 'محمد تشکری',
  //   role: 'مدیر سیستم',
  //   description: '',
  // },
  maani: {
    name: 'مانی بیگی',
    role: 'متخصص علوم داده',
    description: '',
  },
  khashayar: {
    name: 'سید خشایار موسوی',
    role: 'توسعه دهنده بک‌اند',
    description: '',
  },
  arash: {
    name: 'آرش عبدوس',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  ehsan: {
    name: 'احسان تشکری',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  sara: {
    name: 'سارا پیروزوند',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  fateme: {
    name: 'فاطمه تشکری',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  aidin: {
    name: 'آیدین نعمتی',
    role: 'دواپس',
    description: '',
  },
  bahar: {
    name: 'بهار ایثاری',
    role: 'دواپس',
    description: '',
  },
};
