export const I18N = {
  useCookie: false,
  alwaysRedirect: false,
  locales: [
    // {
    //   code: 'en',
    //   iso: 'en-US',
    //   name: 'English',
    //   file: 'en/index.js',
    // },
    {
      code: 'fa',
      iso: 'fa-IR',
      name: 'فارسی',
      file: 'fa/index.js',
    },
  ],
  lazy: true,
  detectBrowserLanguage: false,
  seo: false,
  langDir: '/locales/',
  defaultLocale: 'fa',
  parsePages: true,
  strategy: 'prefix_and_default',
};
