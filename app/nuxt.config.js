import path from 'path';
import fs from 'fs';
import MarkdownIt from 'markdown-it';
import FMMode from 'frontmatter-markdown-loader/mode';
import { I18N } from './locales/i18n-config';

function getMdPath(lang, type) {
  let initLang = lang;
  if (lang === 'fa') {
    initLang = '';
  }
  return fs
    .readdirSync(path.resolve(__dirname, 'contents', `${lang}/${type}`))
    .filter((filename) => path.extname(filename) === '.md')
    .map((filename) => `${initLang}/${type}/${path.parse(filename).name}`);
}

const md = new MarkdownIt({
  html: true,
  typographer: true,
});

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  router: {
    base: '/',
    middleware: 'direction',
  },

  server: {
    port: process.env.NUXT_PORT,
    host: `${process.env.NUXT_HOST}`,
  },
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  loading: {
    color: '#ff9e31',
    height: '4px',
  },
  head: {
    link: [
      { rel: 'author', href: 'humans.txt' },
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicons/favicon.ico',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/favicon-16.png',
        sizes: '16x16',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/favicon-32.png',
        sizes: '32x32',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-36.png',
        sizes: '36x36',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-48.png',
        sizes: '48x48',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-72.png',
        sizes: '72x72',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-144.png',
        sizes: '144x144',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-192.png',
        sizes: '192x192',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-256.png',
        sizes: '256x256',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-512.png',
        sizes: '512x512',
      },
      {
        rel: 'apple-touch-icon',
        href: '/favicons/apple-touch-icon.png',
      },
    ],
  },
  /*
   ** Global CSS
   */
  css: [
    '@aasaam/noto-font/dist/font-face.css',
    '@aasaam/brand-icons/font/font-face.css',
    '~/assets/styles/main.scss',
    'aos/dist/aos.css',
  ],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    '~/plugins/swiper.js',
    { src: '~/plugins/directives.js', ssr: false },
    { src: '~/plugins/leaflet.js', ssr: false },
    { src: '~/plugins/wow.js', ssr: false },
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxtjs/dotenv', '@nuxtjs/axios', ['nuxt-i18n', I18N]],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    optionsPath: './plugins/vuetify.js',
    defaultAssets: false,
    treeShake: true,
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    extend(config, { isDev, isClient, loaders: { vue } }) {
      if (isClient) {
        vue.transformAssetUrls.img = ['data-src', 'src'];
        vue.transformAssetUrls.source = ['data-srcset', 'srcset'];
      }
      config.module.rules.push({
        test: /\.md$/,
        loader: 'frontmatter-markdown-loader',
        include: path.resolve(__dirname, 'contents'),
        options: {
          mode: [FMMode.VUE_COMPONENT, FMMode.VUE_RENDER_FUNCTIONS],
          vue: {
            root: 'markdown-body',
          },
          markdown(body) {
            return md.render(body);
          },
        },
      });
      config.node = {
        fs: 'empty',
      };
    },
  },
  generate: {
    routes: ['/en', '404']
      .concat(getMdPath('fa', 'blog'))
      .concat(getMdPath('en', 'blog')),
  },
};
