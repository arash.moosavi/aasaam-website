import Vue from 'vue';
import scrollAnime from '~/directives/scroll';

Vue.directive('scrollAnime', scrollAnime);
