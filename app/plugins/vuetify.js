// import colors from 'vuetify/es5/util/colors';
import '@mdi/font/css/materialdesignicons.css';

// vuetify.options.js
export default function ({ store }) {
  return {
    icons: {
      iconfont: 'mdi',
    },
    rtl: store.state.rtlMode,
    // rtl: true,
    // theme: {
    //   dark: false,
    //   themes: {
    //     dark: {
    //       primary: colors.blue.darken2,
    //       accent: colors.grey.darken3,
    //       secondary: colors.amber.darken3,
    //       info: colors.teal.lighten1,
    //       warning: colors.amber.base,
    //       error: colors.deepOrange.accent4,
    //       success: colors.green.accent3
    //     }
    //   }
    // }
  };
}
