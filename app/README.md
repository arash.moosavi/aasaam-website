# aasaam website

## Build Setup

```bash
# install dependencies
$ npm install
$ npm run copy:favicons
$ npm run copy:humans
$ npm run manifest

### serve with hot reload at localhost:3000
export NUXT_HOST=0.0.0.0
export NUXT_PORT=3000
$ npm run dev


### Optimizing assets
$ [sudo] npm install -g svgo
$ npm run svgo


# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate

developed with ❤️

```
