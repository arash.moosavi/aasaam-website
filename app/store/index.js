export const mutations = {
  SET_DIRECTION(state, payload) {
    state.rtlMode = payload;
  },
};

// actions for async task. dispatch from component. commit when async done. argument is context.
// context has access to commit method.context have many option inside itself.
// if you want to use only commit , you can use obj destructor. like { commit }
export const actions = {};

export const getters = {
  doubleCounter: (state) => state.counter * 2,
  doneTodos: (state) => state.todos,
  getLangList: (state) => state.locales,
};

export const state = () => ({
  rtlMode: true,
});
