const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');
const flatten = require('flat');
// const person = require('../locales/fa/member.js');
const files = execSync('find ./locales/en -type f -name "*.js"', { encoding: 'utf8' })
  .trim()
  .split('\n');

files.forEach((a) => {
  const text = require(`../${a}`);

  // const list = flatten({ text });
  const rows = [];
  const list = flatten(text);
  console.log(list);
  Object.keys(list).forEach((key) => {
    const value = list[key];
    rows.push(`${key}\t${value}`);
  });
  fs.writeFileSync(`/artmary/encsv/${path.basename(a, '.js')}.csv`, rows.join('\n'));
});
