FROM node:12-buster-slim

# create destination directory

ENV APP_ROOT /app

RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}
ADD ./app ${APP_ROOT}

RUN export DEBIAN_FRONTEND=noninteractive; \
  echo 'cache = "/tmp/npm"' > /root/.npmrc; \
  apt-get update -y && apt-get -y install g++ build-essential python3 python3-dev \
  && npm install \
  && npm run copy:favicons \
  && npm run copy:humans \
  && npm run manifest \
  && npm run build \
  && rm node_modules -rf \
  && npm install --production \
  && apt purge g++ build-essential python3 python3-dev -y \
  && apt autoremove -y && apt-get clean \
  && rm -r /var/lib/apt/lists/* && rm -rf /tmp && mkdir /tmp && chmod 777 /tmp && truncate -s 0 /var/log/*.log

# expose 5000 on container
EXPOSE 5000

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=5000

# start the app
CMD [ "npm", "start" ]
